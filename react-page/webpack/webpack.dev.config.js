var webpack = require('webpack');
var path = require('path');

module.exports = {
	entry: [
		path.join(__dirname, '../index.js')
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015', 'react']
				}
			},
			{
				test: /\.less$/,
				loaders: ["style-loader", "css-loader", "less-loader"]
			},
			{ 
				test: /\.css$/,
                loaders: [ 'style-loader', 'css-loader' ]
			},
			{
				test: /\.(?:png|jpg|svg)$/,
				loader: 'url-loader',
				query: {
				  limit: 10000
				}
			  }
		]
	},
	resolve : {
		extensions: ['.js', '.jsx'],
		alias : {
			Images : path.resolve(__dirname, '../res', 'Images'),
			Res : path.resolve(__dirname, '../res')
		}
	},
	output: {
		path: path.join(__dirname, '../dist'),
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: path.join(__dirname, '../dist'),
		historyApiFallback: true
	},
	mode : 'development'
}