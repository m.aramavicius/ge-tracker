import React from 'react';
import ReactDOM from 'react-dom';

import Site from './containers/Site';
import Axios from 'axios';


window.data = {

	call : [],
	orders : null,
	loaded : false
}

Axios.get('http://localhost:3000/data')
	.then( (response)=>{
		
		window.data.orders = response.data.orders;
		window.data.loaded = true;

		for ( var i = 0; i < window.data.call.length; i++ ) {
			try{
				window.data.call[i]();
			}
			catch(e){
				console.log(e);
			}
		}

		window.data.call = [];
	});

ReactDOM.render(<Site />, document.getElementById('app'))

