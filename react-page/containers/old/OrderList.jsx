
import React, {Component} from 'react';
import core from './Core';
import './table.css';

export default class OrderList extends Component {


	create () {

		let table = []

		var table_head = [
			<td key={0} className='header'>Name</td>,
			<td key={1} className='header'>Buy Price</td>,
			<td key={2} className='header'>Sell Price</td>,
			<td key={3} className='header'>Count</td>,
			<td key={4} className='header'>Profit</td>
		];
		table.push(<tr key={0}>{table_head}</tr>);


		for ( let i = 0; i < window.g.data.orders.length; i++ ) {

			table.push(this.makeRow(window.g.data.orders[i], i+1));
		}
		
		return table;
	}

	makeRow(order,key) {
		let children = [];

		var profit = order.sell * order.count - order.buy * order.count;

		var numberStyle = {
			textAlign : 'right'
		};

		children.push(<td key={0} className='cell'>{order.name}</td>);
		children.push(<td key={1} style={numberStyle} className='cell'>{core.format_number(order.buy)}</td>);
		children.push(<td key={2} style={numberStyle} className='cell'>{core.format_number(order.sell)}</td>);
		children.push(<td key={3} style={numberStyle} className='cell'>{core.format_number(order.count)}</td>);
		children.push(<td key={4} style={numberStyle} className='cell'>{core.format_number(profit)}</td>);
	
		return <tr key={key}>{children}</tr>
	}


    render () {
        return (<table  border="1">
			<tbody>
				{this.create()}
			</tbody>
		</table>)
    }
}