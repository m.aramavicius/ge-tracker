
import React, {Component} from 'react';
import core from './Core';

export default class OrderHeader extends Component {

	style() {

		return {
			height : "4em",
			textAlign : "center"
		}
	}

	getProfit() {

		var profit = 0;

		for ( var i = 0; i < g.data.orders.length; i++ ) {

			var order = g.data.orders[i];
			profit += order.count * order.sell - order.count * order.buy;
		}

		return profit;
	}

    render () {
        return (<div style={this.style()}>TOTAL PROFIT<br></br>{core.format_number(this.getProfit())}</div>)
    }
}