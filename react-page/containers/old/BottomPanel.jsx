
import React, {Component} from 'react';
import axios from "axios";

export default class OrderHeader extends Component {

	style () {

		return {
			width:"100%",
			height:"1.5em",
			verticalAlign:"middle",
			lineHeight:"1.5em"
		}
	}

    render () {
        return (<div style={this.style()}>
			Name <input id='name'></input>
			Buy Price <input id='buy'></input>
			Sell Price <input id='sell'></input>
			Count <input id='count'></input>
			<button onClick={this.sendData}>Add Order</button>
		</div>)
	}
	
	sendData(){
		
		var name = document.getElementById('name').value;
		var buy = document.getElementById('buy').value;
		var sell = document.getElementById('sell').value;
		var count = document.getElementById('count').value;

		if ( name.length <= 0 || buy.length <= 0 || sell <= 0 || count <= 0 ) {

			console.log("BAD INPUT");
			return;
		}
		
		var orderid = 0;

		if ( g.data.orders.length > 0 ) {
			
			orderid = g.data.orders[g.data.orders.length-1].order_id+1;

		}
		var date = new Date();

		var data = {
			name : name.toLocaleLowerCase(),
			buy : Number(buy),
			sell : Number(sell),
			count : Number(count),
			id : -1,
			timestamp : date.getTime(),
			order_id : orderid
		};
		
		var send = JSON.stringify(data);

		axios.post('http://localhost:3000/update',data);
	}
}