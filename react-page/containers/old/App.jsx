
import React, {Component} from 'react';
import OrderList from "./OrderList";
import OrderHeader from "./OrderHeader";
import BottomPanel from "./BottomPanel";
import Menu from './Menu';

export default class App extends Component {

    style() {

        return {
            margin:'auto',
            width:"80%",
            background: "lightblue"
        };

    }

    render () {
        return <div style={this.style()}>
            <Menu></Menu>
            <OrderHeader></OrderHeader>
            <OrderList></OrderList>
            <BottomPanel></BottomPanel>
        </div>
    }
}