
import React, {Component} from 'react';

export default class App extends Component {

    style() {

        return {
			width:"100%",
			height:'10em',
            background: "black"
        };

    }

    render () {
        return <div style={this.style()}>
        </div>
    }
}