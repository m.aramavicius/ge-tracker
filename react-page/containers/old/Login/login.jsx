
import React, {Component} from 'react';
import './login.css';
import axios from "axios";

export default class Login extends Component {

    style() {

        return {
            margin:'auto',
			background: "lightblue",
			position: 'absolute',
			transform :'translate(-50%,-50%)',
			top:'50%',
            left : '50%',
            padding : '2em'
        };

    }

    render () {
        return <div style={this.style()}>
        
            <div className='header'>GE TRACKER LOGIN</div>
            <form onSubmit={this.DoLogin}>
			<input id='username'></input>
			<input id='password' type='password'></input>
			<button>LOGIN</button>
            </form>
        </div>
    }

    DoLogin(e) {
        e.preventDefault();

        var uname = document.getElementById('username');
        var pword = document.getElementById('password');

        axios.post('http://localhost:3000/login', { username : uname.value, password : pword.value })
        	 .then( response => {
             
                console.log(response);
             });
    }
}