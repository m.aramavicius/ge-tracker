
// export default 
const core = {

	format_number : function format_number(num){ 

		var t = String(num);
		var str = "";
	
		var counter = 0;
	
		for ( var i = t.length-1; i >= 0; i-- ) {
	
			counter ++;
			str = t[i] + str;
	
			if ( counter >= 3 ) {
				str = " " + str;
				counter = 0;
			}
	
		}
	
		return str;
	}

};

export default core;