import React, {Component} from 'react';
import '../styles/main.css';
import Logo from './Menu/Logo';
import Dashboard from './Content/Dashboard';
import { ContentState } from './Content';
import { SiteState } from '../Site';

export var MENU = Object.freeze({
	DASHBOARD : 0,
	ALL_TRADES : 1,
	ANALYTICS : 2,
	MOST_PROFITABLE : 3,
	ADD_TRADE : 4,
	LOG_OUT : 5
});

export default class App extends Component {

	constructor(props) {

		super(props);
		this.menu = React.createRef();

		this.toggle = this.toggle.bind(this);

		this.state = {

			full : true
		};
	}

    render () {
        return (
		<div id='menu' ref={this.menu}>
			<Logo full={this.state.full}></Logo>
			<button className='menu-toggle' onClick={this.toggle}>
				{this.state.full ? '<' : '>'}
			</button>

			{this.getElements()}
        </div>
		)
	}
	
	toggle() {

		var menu = this.menu.current;
		
		if ( menu.className == '' ) {
			menu.className = 'hide';
			this.setState({
				full : false
			});
		}
		else {
			menu.className = '';
			this.state.full = true;
			this.setState({
				full:true
			});
		}
	}

	getElements() {
		
		var elements = [];
		elements.push(<div className='menu-spacer' key={0}></div>);

		var last = 0;

		if ( this.state.full ) {

			var addMenuItem = function(key, text, onclick) {
				elements.push(<div className='menu-item-shown' key={key} onClick={onclick}>{text}</div>);
			}


			addMenuItem(1,"Dashboard", this.onMenuClick.bind(this,MENU.DASHBOARD));
			addMenuItem(2,"All Trades", this.onMenuClick.bind(this,MENU.ALL_TRADES));
			addMenuItem(3,"Analytics", this.onMenuClick.bind(this,MENU.ANALYTICS));
			addMenuItem(4,"Most Profitable", this.onMenuClick.bind(this,MENU.MOST_PROFITABLE));

			elements.push(<div className='menu-spacer' key={5}></div>);

			addMenuItem(6, "Add Trade", this.onMenuClick.bind(this,MENU.ADD_TRADE));
			addMenuItem(7, "Log Out", this.onMenuClick.bind(this,MENU.LOG_OUT));

			last = 7;
		}
		else {


			var addMenuItem = function(key, img, onclick) {
				elements.push(<div className='menu-item-hidden' key={key} onClick={onclick}>
					<img src={img}></img>
				</div>);
			}

			addMenuItem(1,require('Images/icon_dashboard.png'), this.onMenuClick.bind(this,MENU.DASHBOARD));
			addMenuItem(2,require('Images/icon_trade.png'), this.onMenuClick.bind(this,MENU.ALL_TRADES));
			addMenuItem(3,require('Images/icon_analytics.png'), this.onMenuClick.bind(this,MENU.ANALYTICS));
			addMenuItem(4,require('Images/icon_money.png'), this.onMenuClick.bind(this,MENU.MOST_PROFITABLE));

			elements.push(<div className='menu-spacer' key={5}></div>);

			addMenuItem(6,require('Images/icon_add.png'), this.onMenuClick.bind(this,MENU.ADD_TRADE));
			addMenuItem(7,require('Images/icon_exit.png'), this.onMenuClick.bind(this,MENU.LOG_OUT));

			last = 7;
		}

		var k = last+1;



		return elements;
	}

	onMenuClick(item) {


		if ( item == MENU.ADD_TRADE ) {
			SiteState.SetPopupOpen(true);
		}
		else {
			ContentState.SetContentState(item);
		}
	}
}