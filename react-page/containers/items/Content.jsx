import React, {Component} from 'react';
import '../styles/main.css';
import Dashboard from './Content/Dashboard';
import AllTrades from './Content/AllTrades';

export var ContentState = {

    instance : null,
    SetContentState:function(state) {

        console.log(this.instance);
        this.instance.setState({
            open_menu : state
        });
    }

}

export default class Content extends Component {

    constructor(props){
        super(props);

        ContentState.instance = this;

        this.state = {
            open_menu : 0
        };
    }

    render () {
        return (
		<div className='content'>
            {this.createContent()}
        </div>
		)
    }
    
    createContent() {

        if ( this.state.open_menu == 0 ) {
            return <Dashboard></Dashboard>
        }
        else if ( this.state.open_menu == 1 ) {
            return <AllTrades></AllTrades>
        }

    }
};