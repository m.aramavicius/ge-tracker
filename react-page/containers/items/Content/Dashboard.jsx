import React, {Component} from 'react';
import '../../styles/main.css';

export default class Dashboard extends Component {

    constructor(props){
        super(props);

        this.state = {
            loaded:false
        };
    }

    render () {
        return (
		<div className='dashboard'>
            <div className='dashboard-header'>DASHBOARD</div>
            <div className='dashboard-panel'>
                <div className='dashboard-panel-header'>
                    Latest Trades
                </div>
                <div className='dashboard-panel-content'>
                    {this.LoadLatestTrades()}
                </div>
            </div>
            <div className='dashboard-panel'>
                <div className='dashboard-panel-header'>
                    Statistics
                </div>
            </div>
        </div>
		)
    }
    
    LoadLatestTrades() {

        if ( window.data.loaded == false ) {
            
            window.data.call.push(
                this.setState.bind(this,{loaded : true})
            );

            return "FETCHING DATA...";
        }
        else if ( this.state.loaded == false ) {
            this.state.loaded = true;
        }

        var add = function(to, text, i, classname) {
            to.push(<td className={classname} key={i}>{text}</td>);
        }
        var header = [];

        add(header, "Name",0);
        add(header, "Buy Price",1, 'dashboard-table-number');
        add(header, "Sell Price",2, 'dashboard-table-number');
        add(header, "Count",3, 'dashboard-table-number');
        add(header, "Profit",4, 'dashboard-table-number');



        var table = [];

        table.push(<tr key={0} className='dashboard-table-header'>{header}</tr>);

        var orders = window.data.orders;
        var ordersToLoad = orders.length >= 10 ? 10 : orders.length;
        
        for ( var i = 0; i < ordersToLoad; i++ ) {
            var row = [];
            var order = orders[ orders.length-(i+1)];
            
            var profitGP = order.sell * order.count - order.buy * order.count;
            var profitPRC = ((order.sell * order.count) / (order.buy * order.count) - 1 ) * 100;
            profitPRC = Math.round(profitPRC * 100) / 100;


            add(row, order.name,0);
            add(row, order.buy, 1, 'dashboard-table-number');
            add(row, order.sell, 2, 'dashboard-table-number');
            add(row, order.count, 3, 'dashboard-table-number');
            add(row, profitGP+" ("+profitPRC+"%)", 4, 'dashboard-table-number');

            table.push(<tr key={1}>{row}</tr>);
        }


        var tableStyle = {
            width:"100%"
        };

        return (<table style={tableStyle}>{table}</table>)
    }
};