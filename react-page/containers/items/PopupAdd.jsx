
import React, {Component} from 'react';
import { SiteState } from '../Site';


export default class Site extends Component {

    constructor(props) {
        super(props);
    }

    render () {
        return (
		<div className='popup-add'>
			<div className='button-close' onClick={this.onClose.bind(this)}>X</div>
			<div className='popup-header'>ADD NEW TRADE</div>
			<div className='popup-content'>
				{this.createInput()}
			</div>
        </div>
		)
	}
	
	onClose() {
		SiteState.SetPopupOpen(false);
	}

	createInput() {

		var style_overall = {
			width: '100%',
			height: '100%',
			
		};

		var style_single = {
			height : '8em',
			margin : 'auto',
			textAlign : 'center',
			fontSize : '125%'
		};

		var style_table_double = {
			width:'80%',
			margin:'auto'
		}
		var style_double = {
			width:'45%',
			display:'inline-block'
		};

		var style_double_td = {
			fontFamily:'Roboto',
			textAlign:'center'
		}

		var style_button_owner = {
			margin:'auto'
		}
		var style_button =  {
			margin : 'auto'
		}

		return (
			<form>
			<div style={style_overall}>
				<div style={style_single}>
						Item Name
						<input className='popup-name-input'></input>
					</div>
				<div style={style_single}>
					Amount traded
					<input className='popup-amount-input' type='number'></input>
				</div>
				<table style={style_table_double}>
					<tr>
						<td style={style_double_td}>Buy Price<input className='popup-amount-input' type='number'></input></td>
						<td style={style_double_td}>SellPrice<input className='popup-amount-input' type='number'></input></td>
					</tr>
				</table>
				<div style={style_button_owner}>
					<button style={style_button}>ADD</button>
				</div>
			</div></form>
		)
	}
}