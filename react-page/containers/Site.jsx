
import React, {Component} from 'react';
import Menu from './items/Menu';
import './styles/main.css';
import Content from './items/Content';
import PopupAdd from './items/PopupAdd';

export var SiteState = {

    instance : null,
    SetPopupOpen:function(open) {

        console.log(this.instance);
        this.instance.setState({
            popup_add : open
        });
    }

}

export default class Site extends Component {

    constructor(props) {
        super(props)

        this.state = {
            popup_add : false
        };

        SiteState.instance = this;
    }

    render () {
        return (
		<div className='main'>
			<Menu></Menu>
            <Content></Content>
            {this.state.popup_add == true ? <PopupAdd></PopupAdd> : ""}
        </div>
		)
    }
}