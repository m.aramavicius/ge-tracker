import json
import io
import os
import time

data = {}

if os.path.isfile('data.json'):
	with open('data.json', 'r') as f:
		try:
			data = json.load(f)
		except ValueError:
			print 'bad file'



def add_order(name, buy, sell, count):

	if ('orders' in data) == False:
		data['orders'] = []

	lastid = 0
	
	if len(data['orders']) > 0:
		lastid = int(data['orders'][-1]['order_id']) + 1
	
	data['orders'].append({
		'name' : name.lower(),
		'buy' : int(buy),
		'sell' : int(sell),
		'count' : int(count),
		'id' : -1,
		'timestamp' : int(time.time() * 1000),
		'order_id' : lastid
	})

	with open('data.json', 'w') as f:
		json.dump(data,f)

def get_profit():

	if ('orders' in data) == False:
		return 0
	
	profit = 0

	for o in data['orders']:
		count = int(o['count'])
		buy = int(o['buy'])
		sell = int(o['sell'])

		profit += sell * count - buy * count
	
	return profit

def print_all_orders():
	if ('orders' in data) == False:
		return 0
	

	for o in data['orders']:
		count = int(o['count'])
		buy = int(o['buy'])
		sell = int(o['sell'])
		name = str(o['name'])

		print '- ', name, ': { BUY = ', buy, ', SELL = ', sell, ', COUNT = ', count, ', PROFIT = ', (sell * count - buy * count), '}'
	

def menu():
	print 'GE MERCH TRACKER'
	print '1 : Add trade'
	print '2 : Print profit'
	print '3 : Print all orders'
	print '4 : Exit'

	inp = raw_input('$: ')

	if IsInt(inp) == False:
		print('--- BAD INPUT (Not correct type) ---')
		menu()
	else:
		inp = int(inp)
		if inp == 1:
			name = raw_input('Name = ')
			buy = raw_input('Buy = ')
			sell = raw_input('Sell = ')
			count = raw_input('Count = ')

			if IsString(name) and IsInt(buy) and IsInt(sell) and IsInt(count):
				add_order(name,buy,sell,count)
			else:
				print('--- BAD INPUT (Values entered incorrectly) ---')

			menu()
		elif inp == 2:
			print '--- TOTAL PROFIT : ', get_profit()
			menu()
		elif inp == 3:
			print_all_orders()
			menu()
		elif inp == 4:
			return
		else:
			print('--- BAD INPUT (Unrecognized command) ---')
			menu()

def IsInt(value):
	try:
		int(value)
	except ValueError:
		return False
	
	return True

def IsString(value):
	try:
		str(value)
	except ValueError:
		return False
	return True

menu()