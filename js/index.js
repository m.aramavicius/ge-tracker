
var g = {

	jsonData : {},
	

	DOM_main : null
};



window.onload = () => {

	core.get('./data', (data)=>{
		g.jsonData = JSON.parse(data);
		LoadSite();
	});


	g.DOM_main = document.getElementById('main');
}



function LoadSite() {

	g.DOM_main.innerHTML = "";

	CreateOrderList();
	CreateUI();
}

function CreateOrderList() {

	var profit = 0;

	var table = document.createElement('table');
	table.border = 1;
	{
		var header = document.createElement('tr');

		var addTD = (text)=> {
			var td = document.createElement('td');
			td.innerHTML = text;
			header.appendChild(td);
		}

		addTD("Name");
		addTD("Buy");
		addTD("Sell");
		addTD("Count");
		addTD("Profit");

		table.appendChild(header);
	}

	for ( var i = 0 ; i < g.jsonData.orders.length; i++ ) {

		var line = g.jsonData.orders[i];
		var p = line.sell * line.count - line.buy * line.count;
		profit += p;
	
		var tr = document.createElement('tr');

		AddOrder(line,tr,p);

		table.appendChild(tr);
	}
	var footer = document.createElement('tr');

	var td = document.createElement('td');
	td.innerHTML = 'TOTAL PROFIT';
	td.align = 'right';
	td.colSpan = 4;
	footer.appendChild(td);

	td = document.createElement('td');
	td.innerHTML = core.format_number(profit);
	td.align = 'right';
	footer.appendChild(td);

	table.appendChild(footer);

	g.DOM_main.appendChild(table);

	return profit;
}

function AddOrder(order,table, profit) {



	var td = document.createElement('td');
	td.innerHTML = order.name;
	table.appendChild(td);

	td = document.createElement('td');
	td.innerHTML = core.format_number(order.buy);
	td.align = 'right';
	table.appendChild(td);


	td = document.createElement('td');
	td.innerHTML = core.format_number(order.sell);
	td.align = 'right';
	table.appendChild(td);

	td = document.createElement('td');
	td.innerHTML = order.count;
	td.align = 'right';
	table.appendChild(td);

	td = document.createElement('td');
	td.align = 'right';
	td.innerHTML = core.format_number(profit);
	table.appendChild(td);

}

function CreateUI() {

	var dom = document.createElement('div');
	
	var add = (text)=>{
		
		var inp = document.createElement('input');
		dom.appendChild(document.createTextNode(text));
		dom.appendChild(inp);
		return inp
	}

	var inpName = add('Name');
	
	var inpBuy = add(' Buy Price');
	inpBuy.type = 'number';

	var inpSell = add(' Sell Price');
	inpSell.type = 'number';

	var inpCount = add(' Count');
	inpCount.type = 'number';
	
	var b = document.createElement('button');
	b.innerHTML = 'Add trade';
	dom.appendChild(b);

	b.onclick = ()=> {
		
		var name = inpName.value;
		var buy = inpBuy.value;
		var sell = inpSell.value;
		var count = inpCount.value;

		if ( name.length <= 0 || buy.length <= 0 || sell <= 0 || count <= 0 ) {

			console.log("BAD INPUT");
			return;
		}
		
		var orderid = 0;

		if ( g.jsonData.orders.length > 0 ) {
			
			orderid = g.jsonData.orders[g.jsonData.orders.length-1].order_id+1;

		}
		var date = new Date();

		var data = {
			name : name.toLocaleLowerCase(),
			buy : Number(buy),
			sell : Number(sell),
			count : Number(count),
			id : -1,
			timestamp : date.getTime(),
			order_id : orderid
		};
		
		var send = JSON.stringify(data);

		core.post('./update',send,()=>{
			console.log("DATA SENT");
		});
	};


	g.DOM_main.appendChild(dom);

}