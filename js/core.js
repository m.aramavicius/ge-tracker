
var core = { 

	get : function (url, callback) {
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = function() { 
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
				callback(xmlHttp.responseText);
		}
		xmlHttp.open("GET", url, true); // true for asynchronous 
		xmlHttp.send(null);
	},
	post : function(url, data, callback) {
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = function() { 
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
				callback(xmlHttp.responseText);
		}
		xmlHttp.open("POST", url, true); // true for asynchronous 
		xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8")

		xmlHttp.send(data);

	},
	format_number : function(num) {

		var t = String(num);
		var str = "";
	
		var counter = 0;
	
		for ( var i = t.length-1; i >= 0; i-- ) {
	
			counter ++;
			str = t[i] + str;
	
			if ( counter >= 3 ) {
				str = " " + str;
				counter = 0;
			}
	
		}
	
		return str;
	}
	
}