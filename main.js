const express = require('express')
const bodyparser = require('body-parser');
const app = express()
const port = 3000
const path = require('path')
const fs = require('fs');

app.use(bodyparser.json());


app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
  });
  

app.get('/', (req,res) => res.sendFile(path.join(__dirname, 'web', 'index.html')))
app.get('/data', (req,res)=>res.sendFile(path.join(__dirname, 'data.json')))

app.post('/login', (req,res)=>{

	console.log(req.body);

	var details = req.body;

	if ( details.username == 'borisas' && details.password == '0005312a' ) {

		res.end("1");
		return;
	}
	res.end("0");
});

app.post('/update', (req,res)=>{

	var data = req.body;

	var file = JSON.parse(fs.readFileSync(path.join(__dirname, 'data.json')), 'utf8');

	file.orders.push(data);

	fs.writeFile(path.join(__dirname, 'data.json'),JSON.stringify(file), ()=>{});

	res.end();
});

app.use(express.static('web'))
app.use(express.static('js'))


app.listen( port, () => {

	console.log("Server online on ${port}");
});